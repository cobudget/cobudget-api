require 'faker'

### TIMEZONES

utc_offsets = []

### USERS

admin = User.create(name: 'Admin', email: 'admin@example.com', password: 'password', utc_offset: -480, joined_first_group_at: DateTime.now.utc, is_super_admin: true) # oaklander
admin.confirm!
puts "generated confirmed admin account email: 'admin@example.com', password: 'password'"

### GROUPS

groups = []

group = Group.create!(name: 'CoTech', created_at: DateTime.now.utc)
group.add_admin(admin)
groups << group

puts "generated CoTech group"
puts "added admin to group"

puts 'Seed: Complete!'
