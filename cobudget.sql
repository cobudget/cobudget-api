--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accounts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE accounts (
    id integer NOT NULL,
    group_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE accounts_id_seq

    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE accounts_id_seq OWNED BY accounts.id;


--
-- Name: allocations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE allocations (
    id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    user_id integer,
    amount numeric(12,2) NOT NULL,
    group_id integer
);


--
-- Name: allocations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE allocations_id_seq

    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: allocations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE allocations_id_seq OWNED BY allocations.id;


--
-- Name: announcement_trackers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE announcement_trackers (
    id integer NOT NULL,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    last_seen timestamp without time zone
);


--
-- Name: announcement_trackers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE announcement_trackers_id_seq

    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: announcement_trackers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE announcement_trackers_id_seq OWNED BY announcement_trackers.id;


--
-- Name: announcements; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE announcements (
    id integer NOT NULL,
    title character varying,
    body text,
    expired_at timestamp without time zone,
    url character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: announcements_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE announcements_id_seq

    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: announcements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE announcements_id_seq OWNED BY announcements.id;


--
-- Name: anomalies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE anomalies (
    id integer NOT NULL,
    tablename text,
    data jsonb,
    reason text,
    who text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: anomalies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE anomalies_id_seq

    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: anomalies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE anomalies_id_seq OWNED BY anomalies.id;


--
-- Name: buckets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE buckets (
    id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    name character varying,
    description text,
    user_id integer,
    target integer,
    group_id integer,
    status character varying DEFAULT 'draft'::character varying,
    funding_closes_at timestamp without time zone,
    funded_at timestamp without time zone,
    live_at timestamp without time zone,
    archived_at timestamp without time zone,
    paid_at timestamp without time zone,
    account_id integer
);


--
-- Name: buckets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE buckets_id_seq

    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: buckets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE buckets_id_seq OWNED BY buckets.id;


--
-- Name: comments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE comments (
    id integer NOT NULL,
    body text NOT NULL,
    user_id integer,
    bucket_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: comments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE comments_id_seq

    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE comments_id_seq OWNED BY comments.id;


--
-- Name: contributions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE contributions (
    id integer NOT NULL,
    user_id integer,
    bucket_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    amount integer NOT NULL
);


--
-- Name: contributions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE contributions_id_seq

    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contributions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE contributions_id_seq OWNED BY contributions.id;


--
-- Name: delayed_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE delayed_jobs (
    id integer NOT NULL,
    priority integer DEFAULT 0 NOT NULL,
    attempts integer DEFAULT 0 NOT NULL,
    handler text NOT NULL,
    last_error text,
    run_at timestamp without time zone,
    locked_at timestamp without time zone,
    failed_at timestamp without time zone,
    locked_by character varying,
    queue character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE delayed_jobs_id_seq

    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE delayed_jobs_id_seq OWNED BY delayed_jobs.id;


--
-- Name: groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE groups (
    id integer NOT NULL,
    name character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    currency_symbol character varying DEFAULT '$'::character varying,
    currency_code character varying DEFAULT 'USD'::character varying,
    customer_id character varying,
    trial_end timestamp without time zone,
    plan character varying
);


--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE groups_id_seq

    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


--
-- Name: memberships; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE memberships (
    id integer NOT NULL,
    group_id integer NOT NULL,
    member_id integer NOT NULL,
    is_admin boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    archived_at timestamp without time zone,
    closed_member_help_card_at timestamp without time zone,
    closed_admin_help_card_at timestamp without time zone,
    status_account_id integer,
    incoming_account_id integer,
    outgoing_account_id integer
);


--
-- Name: memberships_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE memberships_id_seq

    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: memberships_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE memberships_id_seq OWNED BY memberships.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: subscription_trackers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE subscription_trackers (
    id integer NOT NULL,
    user_id integer NOT NULL,
    subscribed_to_email_notifications boolean DEFAULT false,
    email_digest_delivery_frequency character varying DEFAULT 'never'::character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: subscription_trackers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE subscription_trackers_id_seq

    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subscription_trackers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE subscription_trackers_id_seq OWNED BY subscription_trackers.id;


--
-- Name: transactions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE transactions (
    id integer NOT NULL,
    datetime timestamp without time zone,
    from_account_id integer,
    to_account_id integer,
    user_id integer,
    amount numeric(12,2) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE transactions_id_seq

    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE transactions_id_seq OWNED BY transactions.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying,
    last_sign_in_ip character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    name character varying,
    tokens text,
    provider character varying,
    uid character varying,
    confirmation_token character varying,
    utc_offset integer,
    confirmed_at timestamp without time zone,
    joined_first_group_at timestamp without time zone,
    is_super_admin boolean DEFAULT false
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq

    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: accounts id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY accounts ALTER COLUMN id SET DEFAULT nextval('accounts_id_seq'::regclass);


--
-- Name: allocations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY allocations ALTER COLUMN id SET DEFAULT nextval('allocations_id_seq'::regclass);


--
-- Name: announcement_trackers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY announcement_trackers ALTER COLUMN id SET DEFAULT nextval('announcement_trackers_id_seq'::regclass);


--
-- Name: announcements id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY announcements ALTER COLUMN id SET DEFAULT nextval('announcements_id_seq'::regclass);


--
-- Name: anomalies id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY anomalies ALTER COLUMN id SET DEFAULT nextval('anomalies_id_seq'::regclass);


--
-- Name: buckets id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY buckets ALTER COLUMN id SET DEFAULT nextval('buckets_id_seq'::regclass);


--
-- Name: comments id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY comments ALTER COLUMN id SET DEFAULT nextval('comments_id_seq'::regclass);


--
-- Name: contributions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY contributions ALTER COLUMN id SET DEFAULT nextval('contributions_id_seq'::regclass);


--
-- Name: delayed_jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY delayed_jobs ALTER COLUMN id SET DEFAULT nextval('delayed_jobs_id_seq'::regclass);


--
-- Name: groups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY groups ALTER COLUMN id SET DEFAULT nextval('groups_id_seq'::regclass);


--
-- Name: memberships id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY memberships ALTER COLUMN id SET DEFAULT nextval('memberships_id_seq'::regclass);


--
-- Name: subscription_trackers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY subscription_trackers ALTER COLUMN id SET DEFAULT nextval('subscription_trackers_id_seq'::regclass);


--
-- Name: transactions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY transactions ALTER COLUMN id SET DEFAULT nextval('transactions_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: accounts; Type: TABLE DATA; Schema: public; Owner: -
--

COPY accounts (id, group_id, created_at, updated_at) FROM stdin;
1	1	2017-11-29 12:01:40.318143	2017-11-29 12:01:40.318143
2	1	2017-11-29 12:01:40.321834	2017-11-29 12:01:40.321834
3	1	2017-11-29 12:01:40.324114	2017-11-29 12:01:40.324114
\.


--
-- Data for Name: allocations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY allocations (id, created_at, updated_at, user_id, amount, group_id) FROM stdin;
\.


--
-- Data for Name: announcement_trackers; Type: TABLE DATA; Schema: public; Owner: -
--

COPY announcement_trackers (id, user_id, created_at, updated_at, last_seen) FROM stdin;
\.


--
-- Data for Name: announcements; Type: TABLE DATA; Schema: public; Owner: -
--

COPY announcements (id, title, body, expired_at, url, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: anomalies; Type: TABLE DATA; Schema: public; Owner: -
--

COPY anomalies (id, tablename, data, reason, who, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: buckets; Type: TABLE DATA; Schema: public; Owner: -
--

COPY buckets (id, created_at, updated_at, name, description, user_id, target, group_id, status, funding_closes_at, funded_at, live_at, archived_at, paid_at, account_id) FROM stdin;
\.


--
-- Data for Name: comments; Type: TABLE DATA; Schema: public; Owner: -
--

COPY comments (id, body, user_id, bucket_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: contributions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY contributions (id, user_id, bucket_id, created_at, updated_at, amount) FROM stdin;
\.


--
-- Data for Name: delayed_jobs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY delayed_jobs (id, priority, attempts, handler, last_error, run_at, locked_at, failed_at, locked_by, queue, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: -
--

COPY groups (id, name, created_at, updated_at, currency_symbol, currency_code, customer_id, trial_end, plan) FROM stdin;
1	CoTech	2017-11-29 12:01:40.25502	2017-11-29 12:01:40.262559	$	USD	\N	\N	\N
\.


--
-- Data for Name: memberships; Type: TABLE DATA; Schema: public; Owner: -
--

COPY memberships (id, group_id, member_id, is_admin, created_at, updated_at, archived_at, closed_member_help_card_at, closed_admin_help_card_at, status_account_id, incoming_account_id, outgoing_account_id) FROM stdin;
1	1	1	t	2017-11-29 12:01:40.307333	2017-11-29 12:02:19.835997	\N	\N	2017-11-29 12:02:19.729	1	2	3
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY schema_migrations (version) FROM stdin;
20171103164422
20140902080400
20141020034021
20141021013930
20141021014544
20141021015120
20141021020849
20141021021304
20141021023054
20141021023830
20141021025901
20141021030740
20141021040016
20141021043240
20141021044603
20141021064138
20141021105906
20141021110926
20141021112832
20141119233350
20141127034653
20141202041913
20141210003814
20141228211743
20150118014531
20150118015756
20150118045413
20150212051753
20150212062818
20150213022658
20150701020614
20150709052335
20150714235506
20150720232941
20150720233501
20150723024055
20150729021548
20150729023949
20150730025709
20150804094316
20150816061343
20150817092534
20150818071945
20150819090454
20150826052006
20150830070815
20150830072415
20150912210409
20150923193236
20151009190941
20151009191350
20151104171710
20151105204527
20151107025819
20151107030406
20151113081236
20151114110756
20151115221849
20160120125739
20160120130009
20160121112502
20160122052600
20160202055141
20160211044338
20160211050430
20160211052541
20160218191412
20160311035750
20160311100322
20160311213104
20160403023202
20160412044017
20160412073353
20160415193307
20160419140651
20170304013843
20170601185512
20170601185750
20170806223700
20171031000000
20171031204333
20171031204445
20171031204651
20171031204735
20171031213553
20171101072016
20171103102810
20171103111939
\.


--
-- Data for Name: subscription_trackers; Type: TABLE DATA; Schema: public; Owner: -
--

COPY subscription_trackers (id, user_id, subscribed_to_email_notifications, email_digest_delivery_frequency, created_at, updated_at) FROM stdin;
1	1	t	weekly	2017-11-29 12:01:40.236872	2017-11-29 12:01:40.247721
\.


--
-- Data for Name: transactions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY transactions (id, datetime, from_account_id, to_account_id, user_id, amount, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, created_at, updated_at, name, tokens, provider, uid, confirmation_token, utc_offset, confirmed_at, joined_first_group_at, is_super_admin) FROM stdin;
1	joaquim@outlandish.com	$2a$10$kmlpd59x8sNJxZffe6x5xuUjDgzHMOikBUaORfy3/kZGkz6ajVQl.	\N	\N	\N	1	2017-11-29 12:02:14.628654	2017-11-29 12:02:14.628654	::1	::1	2017-11-29 12:01:40.215235	2017-11-29 12:02:37.1122	Admin	{"i_65ujtjYtxBhs0MX_jk4w":{"token":"$2a$10$PHUJunwrHTakvo8GdyK0XeoPAtj/HmB/NE9BuPTglhB4KrSfj3yuK","expiry":1513166556,"last_token":"$2a$10$/OcX0A2JfKQuUncs4HdECum/6TvUOF3GdCBDbMbuxw.PjCKmoG9T2","updated_at":"2017-11-29T12:02:37.111+00:00"}}	email	joaquim@outlandish.com	\N	0	2017-11-29 12:01:40.241825	2017-11-29 12:01:40.112963	t
\.


--
-- Name: accounts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('accounts_id_seq', 3, true);


--
-- Name: allocations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('allocations_id_seq', 1, false);


--
-- Name: announcement_trackers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('announcement_trackers_id_seq', 1, false);


--
-- Name: announcements_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('announcements_id_seq', 1, false);


--
-- Name: anomalies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('anomalies_id_seq', 1, false);


--
-- Name: buckets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('buckets_id_seq', 1, false);


--
-- Name: comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('comments_id_seq', 1, false);


--
-- Name: contributions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('contributions_id_seq', 1, false);


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('delayed_jobs_id_seq', 1, false);


--
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('groups_id_seq', 1, true);


--
-- Name: memberships_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('memberships_id_seq', 1, true);


--
-- Name: subscription_trackers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('subscription_trackers_id_seq', 1, true);


--
-- Name: transactions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('transactions_id_seq', 1, false);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('users_id_seq', 1, true);


--
-- Name: accounts accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (id);


--
-- Name: allocations allocations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY allocations
    ADD CONSTRAINT allocations_pkey PRIMARY KEY (id);


--
-- Name: announcement_trackers announcement_trackers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY announcement_trackers
    ADD CONSTRAINT announcement_trackers_pkey PRIMARY KEY (id);


--
-- Name: announcements announcements_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY announcements
    ADD CONSTRAINT announcements_pkey PRIMARY KEY (id);


--
-- Name: anomalies anomalies_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY anomalies
    ADD CONSTRAINT anomalies_pkey PRIMARY KEY (id);


--
-- Name: buckets buckets_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY buckets
    ADD CONSTRAINT buckets_pkey PRIMARY KEY (id);


--
-- Name: comments comments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: contributions contributions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY contributions
    ADD CONSTRAINT contributions_pkey PRIMARY KEY (id);


--
-- Name: delayed_jobs delayed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY delayed_jobs
    ADD CONSTRAINT delayed_jobs_pkey PRIMARY KEY (id);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: memberships memberships_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY memberships
    ADD CONSTRAINT memberships_pkey PRIMARY KEY (id);


--
-- Name: subscription_trackers subscription_trackers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY subscription_trackers
    ADD CONSTRAINT subscription_trackers_pkey PRIMARY KEY (id);


--
-- Name: transactions transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY transactions
    ADD CONSTRAINT transactions_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: delayed_jobs_priority; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX delayed_jobs_priority ON delayed_jobs USING btree (priority, run_at);


--
-- Name: index_accounts_on_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_accounts_on_group_id ON accounts USING btree (group_id);


--
-- Name: index_allocations_on_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_allocations_on_group_id ON allocations USING btree (group_id);


--
-- Name: index_allocations_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_allocations_on_user_id ON allocations USING btree (user_id);


--
-- Name: index_announcement_trackers_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_announcement_trackers_on_user_id ON announcement_trackers USING btree (user_id);


--
-- Name: index_buckets_on_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_buckets_on_group_id ON buckets USING btree (group_id);


--
-- Name: index_buckets_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_buckets_on_user_id ON buckets USING btree (user_id);


--
-- Name: index_comments_on_bucket_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_comments_on_bucket_id ON comments USING btree (bucket_id);


--
-- Name: index_comments_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_comments_on_user_id ON comments USING btree (user_id);


--
-- Name: index_contributions_on_bucket_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_contributions_on_bucket_id ON contributions USING btree (bucket_id);


--
-- Name: index_contributions_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_contributions_on_user_id ON contributions USING btree (user_id);


--
-- Name: index_memberships_on_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_memberships_on_group_id ON memberships USING btree (group_id);


--
-- Name: index_memberships_on_member_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_memberships_on_member_id ON memberships USING btree (member_id);


--
-- Name: index_subscription_trackers_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_subscription_trackers_on_user_id ON subscription_trackers USING btree (user_id);


--
-- Name: index_transactions_on_from_account_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_transactions_on_from_account_id ON transactions USING btree (from_account_id);


--
-- Name: index_transactions_on_to_account_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_transactions_on_to_account_id ON transactions USING btree (to_account_id);


--
-- Name: index_transactions_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_transactions_on_user_id ON transactions USING btree (user_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: memberships fk_rails_09aa8b45c2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY memberships
    ADD CONSTRAINT fk_rails_09aa8b45c2 FOREIGN KEY (outgoing_account_id) REFERENCES accounts(id);


--
-- Name: buckets fk_rails_5b923e12ec; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY buckets
    ADD CONSTRAINT fk_rails_5b923e12ec FOREIGN KEY (account_id) REFERENCES accounts(id);


--
-- Name: transactions fk_rails_77364e6416; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY transactions
    ADD CONSTRAINT fk_rails_77364e6416 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: accounts fk_rails_94b396a82e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT fk_rails_94b396a82e FOREIGN KEY (group_id) REFERENCES groups(id);


--
-- Name: memberships fk_rails_a50d6e540c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY memberships
    ADD CONSTRAINT fk_rails_a50d6e540c FOREIGN KEY (status_account_id) REFERENCES accounts(id);


--
-- Name: memberships fk_rails_a87348158d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY memberships
    ADD CONSTRAINT fk_rails_a87348158d FOREIGN KEY (incoming_account_id) REFERENCES accounts(id);


--
-- Name: transactions fk_rails_dfd3db5073; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY transactions
    ADD CONSTRAINT fk_rails_dfd3db5073 FOREIGN KEY (to_account_id) REFERENCES accounts(id);


--
-- Name: transactions fk_rails_f11d9eb2ff; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY transactions
    ADD CONSTRAINT fk_rails_f11d9eb2ff FOREIGN KEY (from_account_id) REFERENCES accounts(id);


--
-- PostgreSQL database dump complete
--

